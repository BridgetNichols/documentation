---
id: devdocs_federation
layout: guide
timer: false
---

Each PeerTube instance is able to fetch video from other compatible servers it follows, in a process known as “federation”.
Federation is implemented using the ActivityPub protocol, in order to leverage existing tools and be
compatible with other services such as Mastodon, Pleroma and [many more](https://fediverse.network/).

Federation in PeerTube is twofold: videos metadata are shared as activities for inter-server communication
in what amounts to sharing parts of one's database, and user interaction via comments which are compatible
with the kind of activity textual platforms like Mastodon use.

<div class="jumbotron jumbotron-fluid" style="margin-top:50px;">
  <div class="container">
    <a id="toc-supported-activities" href="#toc-supported-activities">
      <h1 class="display-4">Supported Activities</h1>
    </a>
    <hr class="my-4">
    <p>The current document is not exhaustive and explains PeerTube's implementation of the ActivityPub specification, as well
    as the extensions made to it using ActivityStreams vocubulary.</p>
    <p class="lead">
      <a class="btn btn-secondary btn-lg" href="https://www.w3.org/TR/activitypub/" role="button">ActivityPub</a>
      <a class="btn btn-secondary btn-lg" href="https://www.w3.org/TR/activitystreams-vocabulary/" role="button">ActivityStreams</a>
    </p>
  </div>
</div>

- [Create](#toc-create)
- [Update](#toc-update)
- [Delete](#toc-delete)
- [Follow](#toc-follow)
- [Accept](#toc-accept)
- [Announce](#toc-announce)
- [Undo](#toc-undo)
- [Like](#toc-like)
- [Reject](#toc-reject)

Custom activities supported that are not part of the ActivityPub specification:

- [Flag](#toc-flag) (for abuse reports)

Every activity extends the following basic type:

```ts
export interface BaseActivity {
  '@context'?: any[];
  id: string;
  to?: string[];
  cc?: string[];
  actor: string | ActivityPubActor;
  type: ActivityType;
  signature?: ActivityPubSignature;
}
```

### Follow <a class="toc" id="toc-follow" href="#toc-follow"></a>

Follow is an activity standardized in the ActivityPub specification (see [Follow Activity](https://www.w3.org/TR/activitypub/#follow-activity-inbox)).
The Follow activity is used to subscribe to the activities of another actor (a server
subscribing to another server's videos, a user subscribing to another user's videos).

#### Supported on

- Actor URI

### Accept <a class="toc" id="toc-accept" href="#toc-accept"></a>

#### Supported on

- Follow

### Reject <a class="toc" id="toc-reject" href="#toc-reject"></a>

Reject is an activity standardized in the ActivityPub specification (see [Reject Activity](https://www.w3.org/TR/activitypub/#reject-activity-inbox)).

#### Supported on

- Follow

### Undo <a class="toc" id="toc-undo" href="#toc-undo"></a>

Undo is an activity standardized in the ActivityPub specification (see [Undo Activity](https://www.w3.org/TR/activitypub/#undo-activity-inbox)).
The Undo activity is used to undo a previous activity.

#### Supported on

- [Follow](#toc-follow)
- [Like](#toc-like)
- [Create](#toc-create)
  - [Dislike](#toc-dislike)
  - [CacheFile](#toc-cachefile)
- [Announce](#toc-announce)

### Like <a class="toc" id="toc-like" href="#toc-like">

Like is an activity standardized in the ActivityPub specification (see [Like Activity](https://www.w3.org/TR/activitypub/#like-activity-inbox)).

#### Supported on

- [Video](#toc-video)

### Update <a class="toc" id="toc-update" href="#toc-update"></a>

Update is an activity standardized in the ActivityPub specification (see [Update Activity](https://www.w3.org/TR/activitypub/#update-activity-inbox)).
The Update activity is used when updating an already existing object.

#### Supported on

- [CacheFile](#toc-cachefile)
- [Video](#toc-video)
- Actor (Person, Application, Group)

### Create <a class="toc" id="toc-create" href="#toc-create"></a>

Create is an activity standardized in the ActivityPub specification (see [Create Activity](https://www.w3.org/TR/activitypub/#create-activity-inbox)).
The Create activity is used when posting a new object. This has the side effect
that the `object` embedded within the Activity (in the object property) is created.

#### Supported on

- [CacheFile](#toc-cachefile) object
- [Video](#toc-video) object
- Abuse object
- View object
- [Dislike](#toc-dislike) object
- [Note](#toc-note) object (for comments)

#### Example

```json
{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {}
  ],
  "to": ["https://peertube2.cpy.re/accounts/root/activity"],
  "type": "Create",
  "actor": "https://peertube2.cpy.re/accounts/root",
  "object": {}
}
```

### Announce <a class="toc" id="toc-announce" href="#toc-announce"></a>

Announce is an activity standardized in the ActivityPub specification (see [Announce Activity](https://www.w3.org/TR/activitypub/#announce-activity-inbox)).

#### Supported on

- [Video](#toc-video)

#### Example

```json
{
  "type": "Announce",
  "id": "https://peertube2.cpy.re/videos/watch/997111d4-e8d8-4f45-99d3-857905785d05/announces/1",
  "actor": "https://peertube2.cpy.re/accounts/root",
  "object": "https://peertube2.cpy.re/videos/watch/997111d4-e8d8-4f45-99d3-857905785d05",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public",
    "https://peertube2.cpy.re/accounts/root/followers",
    "https://peertube2.cpy.re/video-channels/root_channel/followers"
  ],
  "cc": []
}
```

<div class="jumbotron jumbotron-fluid" style="margin-top:50px;">
  <div class="container">
    <a id="toc-objects" href="#toc-objects">
      <h1 class="display-4">Supported Objects</h1>
    </a>
    <p class="lead">Objects are the core concept around which both ActivityStreams
     and ActivityPub are built. Objects are often wrapped in Activities and are contained in streams of Collections, which are themselves subclasses of Objects.</p>
  </div>
</div>

### Video <a class="toc" id="toc-video" href="#toc-video"></a>

<div class="alert alert-info" role="alert">
  <strong>Note:</strong> this object extends the ActivityPub specification, and therefore some properties are not part of it.
</div>

#### Example

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/video-torrent-object.ts](https://github.com/Chocobozzz/PeerTube/blob/v1.1.0-alpha.2/shared/models/activitypub/objects/video-torrent-object.ts).

```typescript
{% remote_lines https://raw.githubusercontent.com/Chocobozzz/PeerTube/v1.1.0-alpha.2/shared/models/activitypub/objects/video-torrent-object.ts|9|0 %}
```

### CacheFile <a class="toc" id="toc-cachefile" href="#toc-cachefile"></a>

<div class="alert alert-info" role="alert">
  <strong>Note:</strong> this object is not standard to the ActivityPub specification.
</div>

The object is used to represent a cached file. It is usually sent by third-party servers to the origin server hosting a video
file (a resolution from a `Video`), to notify it that they have put up a copy of that file. The origin server should
then add the server emitting the `CacheFile` to the list of [WebSeeds](http://bittorrent.org/beps/bep_0019.html) for that file.

#### Example

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/cache-file-object.ts](https://github.com/Chocobozzz/PeerTube/blob/v1.1.0-alpha.2/shared/models/activitypub/objects/cache-file-object.ts).

```typescript
{% remote_lines https://raw.githubusercontent.com/Chocobozzz/PeerTube/v1.1.0-alpha.2/shared/models/activitypub/objects/cache-file-object.ts|2|0 %}
```

### Note <a class="toc" id="toc-note" href="#toc-note"></a>

A `Note` is usually a comment made to a video. Since most ActivityPub textual platforms use the `Note` object for their
messages, most of them can interact in the same way with PeerTube videos, making them able to comment PeerTube videos
directly! A `Note` is emitted along the Video publication object: the former is used to notify textual platforms of
the Fediverse, the latter to notify the Vidiverse.

#### Example

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/video-comment-object.ts](https://github.com/Chocobozzz/PeerTube/blob/v1.1.0-alpha.2/shared/models/activitypub/objects/video-comment-object.ts).

```typescript
{% remote_lines https://raw.githubusercontent.com/Chocobozzz/PeerTube/v1.1.0-alpha.2/shared/models/activitypub/objects/video-comment-object.ts|2|0 %}
```

### Flag <a class="toc" id="toc-flag" href="#toc-flag"></a>

<div class="alert alert-info" role="alert">
  <strong>Note:</strong> this object is not standard to the ActivityPub specification.
</div>

A `Flag` represents a report transfered to a remote instance.

#### Example

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/video-abuse-object.ts](https://github.com/Chocobozzz/PeerTube/blob/v1.1.0-alpha.2/shared/models/activitypub/objects/video-abuse-object.ts).

```typescript
{% remote_lines https://raw.githubusercontent.com/Chocobozzz/PeerTube/v1.1.0-alpha.2/shared/models/activitypub/objects/video-abuse-object.ts|0|0 %}
```

### Dislike <a class="toc" id="toc-dislike" href="#toc-dislike"></a>

<div class="alert alert-info" role="alert">
  <strong>Note:</strong> this object is not standard to the ActivityPub specification.
</div>

#### Example

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/dislike-object.ts](https://github.com/Chocobozzz/PeerTube/blob/v1.1.0-alpha.2/shared/models/activitypub/objects/dislike-object.ts).

```typescript
{% remote_lines https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/activitypub/objects/dislike-object.ts|0|0 %}
```

_[Fediverse]: several servers following one another, several users following each other. Designates federated communities in general
_[Vidiverse]: same as Fediverse, but federating videos specifically
