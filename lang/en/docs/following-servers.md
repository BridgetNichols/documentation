---
id: docs_following_servers
guide: docs_configuration
layout: guide
additional_reading_tags: ['configuration']
---

Following servers and being followed as a server ensure visibility of your videos on other instances, and visibility of their videos on your instance. Both are important concepts of PeerTube as they allow instances and their users to interact.

<div class="alert alert-info" role="alert">
  <strong>What is a "follow":</strong> a follow is <a href="https://www.w3.org/TR/activitypub/#follow-activity-inbox">a kind of activity</a> in the ActivityPub linguo.
  It allows to subscribe to a server's user activity in the PeerTube realm.
</div>

# Following an instance <a class="toc" id="toc-following-an-instance" href="#toc-following-an-instance"></a>

Following an instance will display videos of that instance on your pages (i.e. "Trending", "Recently Added", etc.) and your users will be able to interact with them.
https://docs.joinpeertube.org/lang/en/docs/install.html#git-stable

## Managing follows <a class="toc" id="toc-managing-follows" href="#toc-managing-follows"></a>

You can add an instance to follow and remove instances you follow in `Administration > Manage Follows > Follow`, and add hostnames of the instances you want to follow there.

![Adding servers to follow](/assets/admin-add-follow.png){:class="img-fluid"}

# Being followed by an instance <a class="toc" id="toc-being-followed-by-an-instance" href="#toc-being-followed-by-an-instance"></a>

Being followed will display videos of your instance on your followers' pages, and their users will be able to interact with your videos.

You cannot yet refuse a follow as it is automatically accepted, but you can block the instance _a posteriori_.
