<div class="install-only-beta" markdown="1">
There is no FreeBSD port of PeerTube available at the time, but there is [a dedicated dependency guide](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/dependencies.md#freebsd) in the official docs!

After that, follow the regular [production guide](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md), which also has FreeBSD specifics.

</div>

<div class="install-only-rc install-only-nightly" markdown="1">
Currently, there are no openSUSE packages available for RC or nightly builds of PeerTube. Please use the tarball:
{% include_relative installations/tarball.md %}
</div>
